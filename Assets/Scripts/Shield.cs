﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shield : MonoBehaviour, IHitBox, IDamager 
{
    [SerializeField] private float health = 3;
    [SerializeField] private float damage = 10f;
    private Text shieldText;
    private bool isShieldActive = false; 
    private bool isShieldCharging = false;
    private float maxHealth;
    public float Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                health = 0f;
                Invoke("Die",0.1f);
            }
        }
    }

    public float Damage
    {
        get => damage;
    }
    public bool IsShieldActive 
    {
        get => isShieldActive;
    }

    public bool IsShieldCharging
    {
        get => isShieldCharging;

        private set
        {
            isShieldActive = value;
        }
    }

    public void Activate(bool status)
    {
        isShieldActive = status;
        if (status == false)
        {
            isShieldCharging = true;
        }
    }
    private void Start()
    {
        maxHealth = health;
        var shieldTextObject = GameObject.FindGameObjectWithTag("ShieldBar");
        if (shieldTextObject != null)
        {
            shieldText = shieldTextObject.GetComponent<Text>();
        }
        isShieldActive = false;
    }

    public void UpdateShield()
    {
        shieldText.text = "SHIELD: " + Health;
    }

    public void Hit(float damage)
    {
        Health -= damage;
        UpdateShield();
    }

    public void Die()
    {
        Debug.LogError("Щит уничтожен");
        isShieldActive = false;
        isShieldCharging = true;
        gameObject.SetActive(false);

    }

    public IEnumerator Charging()
    {
        while (true)
        {
            yield return null;;
            if (!(health >= maxHealth) && isShieldCharging)
            {
                yield return new WaitForSeconds(1f);
                health += 0.5f;
                UpdateShield();
                if (health >= maxHealth)
                {
                    isShieldCharging = false;
                    continue;
                }
                
            }
            else if (health >= maxHealth)
            {
                isShieldCharging = false;
                continue;
            }
        }
    }
}
