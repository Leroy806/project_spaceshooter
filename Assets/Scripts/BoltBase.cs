﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltBase : MonoBehaviour, IHitBox, IDamager 
{
    [SerializeField] private float speed = 20f;
    [SerializeField] private float health = 1;
    [SerializeField] private float damage = 1f;
    [SerializeField] private GameObject getDamageExplosion;

    private Rigidbody rb;
    
    
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Mover.MoveObj(this.gameObject, rb, speed);
    }

    public float Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    public float Damage
    {
        get => damage;
    }

    public void Hit(float damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        if (getDamageExplosion != null)
        {
            var exp = Instantiate(getDamageExplosion, this.transform.position, this.transform.rotation);
            Destroy(exp, 1f);
        }
        this.gameObject.SetActive(false);
    }
}
