﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [HideInInspector]
    public int level;
    [HideInInspector]
    public int score;
    [HideInInspector]
    public string nextLevelType;

    public void Save()
    {
        SaveSystem.Save(this);
    }

    public void Load()
    {
        GameData data = SaveSystem.Load();
        if (data != null)
        {
            level = data.level;
            score = data.score;
            nextLevelType = data.nextLevelType;
        }
    }

    public void Reset()
    {
        SaveSystem.Reset(this);
    }
}
