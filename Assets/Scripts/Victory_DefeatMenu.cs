﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Victory_DefeatMenu : MonoBehaviour
{
    [SerializeField] private LevelLoader levelLoader;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text totalScore;

    private GameController gameController; 

    private void Awake()
    {
        var gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null && scoreText != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
            scoreText.text = "SCORE: " + gameController.Score;
            totalScore.text = "TOTAL SCORE: " + gameController.TotalScore; 
        }
    }

    public void BackToMainMenu()
    {
        Time.timeScale = 1f;
        levelLoader.LoadGameScene(0);
    }

    public void NextLevel()
    {
        Time.timeScale = 1f;
        levelLoader.LoadGameScene(SceneManager.GetActiveScene().buildIndex + 1 );
    }

    public void TryAgain()
    {
        Time.timeScale = 1f;
        levelLoader.LoadGameScene(SceneManager.GetActiveScene().buildIndex);
    }
}
