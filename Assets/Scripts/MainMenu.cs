﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private LevelLoader levelLoader;
    [SerializeField] private Text totalScore;
    [SerializeField] private Text level;

    private Player player;

    private void Start()
    {
        var playerObject = GameObject.FindGameObjectWithTag("Player");
        if (playerObject != null)
        {
            player = playerObject.GetComponent<Player>();
        }
        player.Load();
        totalScore.text = "TOTAL SCORE: " + player.score;
        level.text = "LEVELS COMPLETE: " + player.level; 
    }

    public void PlayGame()
    {
        levelLoader.LoadGameScene(player.level+1);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ResetScore()
    {
        player.Reset();
        player.Load();
        totalScore.text = "TOTAL SCORE: " + player.score;
        level.text = "LEVELS COMPLETE: " + player.level;
    }

}
