﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour, IHitBox, IDamager  
{
    [SerializeField] private float health = 5f;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float speed = 10f;
    [SerializeField] private float tilt = 4f;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Boundary boundary;
    [SerializeField] private GameObject shot;
    [SerializeField] private Transform[] shotSpawns;
    [SerializeField] private float fireRate = 0.25f;
    [SerializeField] private GameObject shield;
    [SerializeField] private GameObject explosion;
    


    private float nextFire;
    private GameController gameController;
    private Text healthText;
    private Shield shieldStatus;
    private List<Transform> shotList = new List<Transform>(5);
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        ObjectsPool.Instance.AddObjects(shot, 10);

        shield.SetActive(false);
        shieldStatus = shield.GetComponent<Shield>();
        shotList.Add(shotSpawns[0]);
        //shotList.Add(shotSpawns[2]);


        var gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        

        var healthTextObject = GameObject.FindGameObjectWithTag("HealthBar");
        if (healthTextObject != null)
        {
            healthText = healthTextObject.GetComponent<Text>();
        }

       

        UpdateHealth();
        shieldStatus.UpdateShield();
        StartCoroutine(shieldStatus.Charging());
    }
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        var movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement*speed;

        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
            );

        rb.rotation = Quaternion.Euler(0f, 0f, rb.velocity.x * -tilt); 
    }

    private void Update()
    {
        if (Input.GetButton("Fire1") && (Time.time > nextFire)) 
        {
            nextFire = Time.time + fireRate;
            foreach (var shotSpawn in shotList)
            {
                /*Quaternion myRotation = Quaternion.identity; 
                myRotation.eulerAngles = new Vector3(0, 0, 0);*/
                ObjectsPool.Instance.GetObject(shot, shotSpawn.position, shotSpawn.rotation);
            }
            GetComponent<AudioSource>().Play();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            if (!shieldStatus.IsShieldCharging && !shieldStatus.IsShieldActive)
            {
                ActivateShield();
            }
            else if (shieldStatus.IsShieldActive)
            {
                DeactivateShield();
            }

        }
    }

    private void ActivateShield()
    {
        shield.SetActive(true);
        shieldStatus.Activate(true);
    }

    private void DeactivateShield()
    {
        shield.SetActive(false);
        shieldStatus.Activate(false);
    }

    public float Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }

    }

    public void Hit(float damage)
    {
        if (!shieldStatus.IsShieldActive)
        {
            Health -= damage;
        }
        UpdateHealth();
    }

    public void Die()
    {
        var exp = Instantiate(explosion, this.transform.position, this.transform.rotation);
        Destroy(exp, 1f);
        this.gameObject.SetActive(false);
        gameController.PlayerDead();
    }

    private void UpdateHealth()
    {
        healthText.text = "HEALTH: " + health;
    }

   public float Damage
    {
        get => damage;
    }
}
