﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SystemRescue_GameController : GameController
{
    [SerializeField] private Text enemyToDestroyText;
    
    private bool isSuperEnemyOnField = false;
    
    private void Start()
    {
        base.Start();
        StartCoroutine(SpawnWaves());
    }
   
    private IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);

        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                var spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                var spawnRotation = Quaternion.identity;

                var spawnNumber = Random.Range(0, hazards.Length);
                if (isSuperEnemyOnField && spawnNumber == 4)
                {
                    spawnNumber--;
                }
                else if (!isSuperEnemyOnField && spawnNumber == 4)
                {
                    isSuperEnemyOnField = true;
                }

                var hazardSpawn = hazards[spawnNumber];
                ObjectsPool.Instance.GetObject(hazardSpawn, spawnPosition, spawnRotation);
                //Instantiate(hazardSpawn, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            isSuperEnemyOnField = false;
            yield return new WaitForSeconds(waveWait);

            
            if (isPlayerDead)
            {
                break;
            }
        }
    }

    public override void UpdateScore()
    {
        base.UpdateScore(); 
        enemyToDestroyText.text = "ENEMY TO DESTROY: " + superEnemyToDestroy;
    }

  
}