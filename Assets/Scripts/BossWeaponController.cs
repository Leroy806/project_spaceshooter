﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWeaponController : MonoBehaviour
{
    [SerializeField] private GameObject[] bolts;
    [SerializeField] private Transform[] shotSpawns;
    [SerializeField] private float fireRate;
    [SerializeField] private float delay;

    private AudioSource audioSource;
    private static int shotCount =0;
    private GameObject player;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }

    private void Fire()
    {
        for (int i = 0; i < shotSpawns.Length; i++)
        {
            if (i == 0 && (shotCount%3)==0)
            {
                Instantiate(bolts[i], shotSpawns[i].position, shotSpawns[i].rotation);
                audioSource.Play();
                
            }
            else if (i !=0 && player != null)
            {
                Vector3 relativePos = player.transform.position - shotSpawns[i].transform.position;
                Instantiate(bolts[i], shotSpawns[i].position, Quaternion.LookRotation(relativePos));
                audioSource.Play();
            }
        }
        shotCount++;
    }
}
