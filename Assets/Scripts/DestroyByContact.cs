﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    //[SerializeField] private GameObject getDamageExplosion;
    [SerializeField] private int scoreValue = 10;

    private GameController gameController;

    private void Start()
    {
        var gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
      
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary" || other.tag == "Enemy" || other.tag == "EnemyBolt")
        {
            return;
        }


        if ((other.tag == "Bolt" && this.tag == "EnemyBolt") || (other.tag == "Bolt" && this.tag == "Player"))
        {
            return;
        }
        /*if (getDamageExplosion != null)
        {
            var exp = Instantiate(getDamageExplosion, this.transform.position, this.transform.rotation);
            Destroy(exp, 1f);
        }*/
        /*if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.PlayerDead();
        }*/

        var target = other.transform.gameObject.GetComponent<IHitBox>();
        var damage = this.transform.gameObject.GetComponent<IDamager>();
        target.Hit(damage.Damage);

        /*if (other.tag != "Shield")*/
        {
            var target1 = this.transform.gameObject.GetComponent<IHitBox>();
            var damage1 = other.transform.gameObject.GetComponent<IDamager>();
            target1.Hit(damage1.Damage);
        }
        if (other.tag == "Bolt")
        {
            gameController.AddScore(scoreValue);
        }
        
    }

}

