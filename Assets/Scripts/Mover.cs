﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
   /* [SerializeField] private float speed=20f;
    private Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;
    }*/

    public static Vector3 MoveObj(GameObject obj, Rigidbody rb, float speed)
    {
        return obj.transform.forward * speed;
    }
}
