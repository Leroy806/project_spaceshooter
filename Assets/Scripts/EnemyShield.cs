﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyShield : MonoBehaviour, IHitBox
{
    [SerializeField] private float health = 10;
    
    private Text enemyShieldText;
    private bool isShieldActive = false; //было true
    private bool isShieldCharging = false;
    private float maxHealth;
    private float lastHit;

    public float Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                health = 0f;
                Invoke("Die", 0.1f);
            }
        }
    }
    void Start()
    {
        maxHealth = health;
        var shieldTextObject = GameObject.FindGameObjectWithTag("EnemyShieldBar");
        if (shieldTextObject != null)
        {
            enemyShieldText = shieldTextObject.GetComponent<Text>();
        }
        lastHit = 0f;
        StartCoroutine(ChargingEnemyShield()); 
    }

    
    public void UpdateEnemyShield()
    {
        enemyShieldText.text = "ENEMY SHIELD: " + Health;
    }

    public void Hit(float damage)
    {
        Health -= damage;
        lastHit = Time.deltaTime;
        Debug.LogError($"удар получен {lastHit}");
        UpdateEnemyShield();
    }

    public void Die()
    {
        gameObject.SetActive(false);
    }

    public IEnumerator ChargingEnemyShield()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            if (!(health >= maxHealth) && (lastHit >= (Time.deltaTime + 5f)))
            {
                health += 0.2f;
                UpdateEnemyShield();
            }
            /*else if (health >= maxHealth)
            {
                isShieldCharging = false;
                continue;
            }*/
        }
    }
}
