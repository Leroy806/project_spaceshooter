﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAsteroid : MonoBehaviour, IHitBox, IDamager
{
    [SerializeField] private float health = 1;
    [SerializeField] private float damage = 1f;
    [SerializeField] private float speed = -5f;
    [SerializeField] private float tumble = 3f;
    [SerializeField] private GameObject explosion;
    [SerializeField] private bool isSuperEnemy = false;
    
    private Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Mover.MoveObj(this.gameObject, rb, speed);
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * tumble;
    }


    public float Speed
    {
        get => speed;
        private set 
        {
            speed = value;
        }
    }
    public float Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    public float Damage
    {
        get => damage;
    }

    public void Accelerate(float accelerate)
    {
        speed -= accelerate;
    }

    public void Hit(float damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        var exp = Instantiate(explosion, this.transform.position, this.transform.rotation);
        Destroy(exp, 1f);
        Destroy(this.gameObject);
    }
}
