﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AsteroidBelt_GameController : GameController 
{ 
[SerializeField] private Vector2 spawnValuesX;
[SerializeField] private Vector2 spawnValuesZ;


private Vector3 spawnValues1;
private Vector3 spawnValues2;
private Quaternion asteroidRotation = Quaternion.identity;
private int waveCount =0;
    
    private void Start()
    {
        base.Start();
        StartCoroutine(SpawnWaves());
    }
    
private IEnumerator SpawnWaves()
{
    yield return new WaitForSeconds(startWait);
    while (true)
    {
            waveCount++;
            int waveType = Random.Range(0,3);
            switch (waveType)
            {
                case 0:
                    spawnValues1.x = spawnValuesX.x;
                    spawnValues2.x = spawnValuesX.y;
                    spawnValues1.y = 0f;
                    spawnValues1.z = spawnValuesZ.y; 
                    spawnValues2.z = spawnValuesZ.y;
                    asteroidRotation.eulerAngles = new Vector3(0, 0, 0);
                    break;
                case 1:
                    spawnValues1.x = spawnValuesX.x;
                    spawnValues2.x = spawnValuesX.x;
                    spawnValues1.y =  0f;
                    spawnValues1.z = spawnValuesZ.x; 
                    spawnValues2.z = spawnValuesZ.y;
                    asteroidRotation.eulerAngles = new Vector3(0, -60, 0);
                    break;
                case 2:
                    spawnValues1.x = spawnValuesX.y;
                    spawnValues2.x = spawnValuesX.y;
                    spawnValues1.y = 0f;
                    spawnValues1.z = spawnValuesZ.x;
                    spawnValues2.z = spawnValuesZ.y;
                    asteroidRotation.eulerAngles = new Vector3(0, 60, 0);
                    break;
            }
        for (int i = 0; i < hazardCount; i++)
        {
            var spawnPosition = new Vector3(Random.Range(spawnValues1.x, spawnValues2.x), spawnValues1.y, Random.Range(spawnValues1.z, spawnValues2.z));
            var spawnRotation = asteroidRotation;
            var hazardSpawn = hazards[Random.Range(0, hazards.Length)];
                //var hazard = Instantiate(hazardSpawn, spawnPosition, spawnRotation);
                var hazard = ObjectsPool.Instance.GetObject(hazardSpawn, spawnPosition, spawnRotation);
                if ((waveCount%3) == 0)
                {
                    var hazardSpeed = hazard.GetComponent<BaseAsteroid>();
                    var accelerate = waveCount / 3;  
                    hazardSpeed.Accelerate(accelerate);
                    //Debug.LogError(hazardSpeed.Speed); 
                }
            yield return new WaitForSeconds(spawnWait);
        }
        yield return new WaitForSeconds(waveWait);

        if (isPlayerDead)
        {
            break;
        }
    }
}


}