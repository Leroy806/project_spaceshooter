﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponController : MonoBehaviour
{
    [SerializeField] private GameObject bolt;
    [SerializeField] private Transform shotSpawn;
    [SerializeField] private float fireRate;
    [SerializeField] private float delay;

    private AudioSource audioSource;

    private Coroutine coroutine;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }


    
    private void Fire()
    {
        Instantiate(bolt, shotSpawn.position, shotSpawn.rotation);
        audioSource.Play();
    }

   
}
