﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossFIght_GameController : GameController
{
    [SerializeField] private Text enemyHealthText;
    [SerializeField] private GameObject bossPrefab;
    [SerializeField] private int waveBeforeBoss = 10;

    private GameObject boss;
    private IHitBox bossHealth;
    private int waveCount;
    private EnemyShipBase bossBase;

    void Start()
    {
        waveCount = waveBeforeBoss;
        base.Start();
       
        StartCoroutine(SpawnWaves());
    }


    private IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);

        while (waveCount != 0)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                var spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                var spawnRotation = Quaternion.identity;

                var spawnNumber = Random.Range(0, hazards.Length);
                var hazardSpawn = hazards[spawnNumber];
                ObjectsPool.Instance.GetObject(hazardSpawn, spawnPosition, spawnRotation);
            //Instantiate(hazardSpawn, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);


            if (isPlayerDead)
            {
                break;
            }

            waveCount--;
        }
        if (waveCount == 0)
        {
            SpawnBoss();
            StopCoroutine(SpawnWaves()); 
        }
    }

    private void SpawnBoss()
    {
        boss = Instantiate(bossPrefab, new Vector3(0f, 0f, 23f), Quaternion.identity);
        bossHealth = boss.GetComponent<IHitBox>();
        Debug.Log(boss.transform.position);
        var newPosition = boss.transform.position;
        UpdateScore();
        bossBase = boss.GetComponent<EnemyShipBase>();
        bossBase.BossEntersTheScene();
    }
    public override void UpdateScore()
    {
        base.UpdateScore();
        if (boss != null) {
            enemyHealthText.text = "ENEMY HEALTH: " + bossHealth.Health;
        }
    }

}

