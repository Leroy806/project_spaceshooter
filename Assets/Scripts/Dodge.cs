﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodge : MonoBehaviour
{
    [SerializeField] private float dodge;
    [SerializeField] private float tilt = 3f;
    [SerializeField] private float smothing;
    [SerializeField] private Vector2 startWait;
    [SerializeField] private Vector2 maneuverTime;
    [SerializeField] private Vector2 maneuverWait;
    [SerializeField] private Boundary boundary;

    private float targetManeuver;
    private Rigidbody rb;
    private float currentSpeed;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        currentSpeed = rb.velocity.z;
        StartCoroutine(Evade());
    }

   

    private IEnumerator  Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

        while (true)
        {
            targetManeuver = Random.Range(1, dodge)* -Mathf.Sign(transform.position.x);
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range (maneuverWait.x, maneuverWait.y));
        }
    }

    private void FixedUpdate()
    {
        var newManeuver = Mathf.MoveTowards(rb.velocity.x, targetManeuver, Time.deltaTime*smothing);
        rb.velocity = new Vector3(newManeuver, 0f, currentSpeed);
        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
            );
        rb.rotation = Quaternion.Euler(0f, 0f, rb.velocity.x * -tilt);
    }
}
