﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int level;
    public int score;
    public string nextLevelType;

    public GameData(Player player)
    {
        level = player.level;
        score = player.score;
        nextLevelType = "1";
        if (level == 0)
        {
            nextLevelType = "1";
        }
        else
        {
            if (level % 4 == 0)
            {
                nextLevelType = "1";
            }
            else if (level % 2 == 0 && level % 4 != 0)
            {
                nextLevelType = "3";
            }
            else if ((level - 1) % 4 == 0)
            {
                nextLevelType = "2";
            }
            else
            {
                nextLevelType = "4";
            }
        }
    }
}
