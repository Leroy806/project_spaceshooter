﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private GameObject loadingImage;
    [SerializeField] private Slider loadingProgress;
    

     public void LoadGameScene(int sceneIndex)
     {
        if (sceneIndex < 5)
        {
            StartCoroutine(LoadAsyncSceneProcess(sceneIndex));
        }
        else
        {
            if (sceneIndex % 4 == 0)
            {
                StartCoroutine(LoadAsyncSceneProcess(4));
            }
            else if (sceneIndex % 2 == 0 && sceneIndex % 4 != 0)
            {
                StartCoroutine(LoadAsyncSceneProcess(2));
            }
            else if ((sceneIndex - 1) % 4 == 0)
            {
                StartCoroutine(LoadAsyncSceneProcess(1));
            }
            else 
            {
                StartCoroutine(LoadAsyncSceneProcess(3));
            }
        }
     }

    public IEnumerator LoadAsyncSceneProcess(int sceneIndex)
    {
        AsyncOperation loadScene = SceneManager.LoadSceneAsync(sceneIndex);
        loadingImage.SetActive(true);

        while (!loadScene.isDone)
        {
            float progress = Mathf.Clamp01(loadScene.progress / 0.9f);

            loadingProgress.value = progress;

            yield return null;
        }
    }
        
    
}
