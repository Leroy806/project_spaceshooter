﻿public interface IDamager
{
    float Damage { get; }
}

public interface IHitBox
{
    float Health { get; }
    void Hit(float damage);
    void Die();
}



