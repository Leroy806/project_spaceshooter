﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] protected string levelType;
    [SerializeField] protected GameObject[] hazards;
    [SerializeField] protected Vector3 spawnValues;
    [SerializeField] protected int hazardCount;
    [SerializeField] protected float spawnWait = 0.5f;
    [SerializeField] protected float startWait = 2f;
    [SerializeField] protected float waveWait = 3f;
    [SerializeField] protected Text scoreText;
    [SerializeField] protected GameObject gameOverText;
    [SerializeField] private float levelTime;
    [SerializeField] protected int superEnemyToDestroy;
    [SerializeField] protected GameObject victoryPanel;
    [SerializeField] protected GameObject defeatPanel;
    [SerializeField] private Slider timeSlider;

    protected int score;
    protected bool isPlayerDead;
    private float timePass;
    private Player player;
    private int totalScore;

    protected void Start()
    {
        var playerObject = GameObject.FindGameObjectWithTag("Player");
        if (playerObject != null)
        {
            player = playerObject.GetComponent<Player>();
            player.Load();
            Debug.Log(player.nextLevelType);
        }
        isPlayerDead = false;
        score = 0;
        UpdateScore();

        for (int i = 0; i < hazards.Length; i++)
        {
            ObjectsPool.Instance.AddObjects(hazards[i], 20);
        }
        victoryPanel.SetActive(false);
        defeatPanel.SetActive(false);

        StartCoroutine(LevelTimeToWinOrLose(levelTime));
    }
    
    private IEnumerator LevelTimeToWinOrLose(float levelTime)
    {
        yield return new WaitForSeconds(1f);

        while (true)
        {
            timePass++;
            timeSlider.value = timePass / levelTime; 
            if ((timePass >= levelTime) && levelTime != 0f)
            {
                if (player.nextLevelType == "1")
                {
                    LevelComplete();
                    victoryPanel.SetActive(true);
                    StopCoroutine(LevelTimeToWinOrLose(levelTime));
                    Time.timeScale = 0f;
                    break;
                }
                defeatPanel.SetActive(true);
                StopCoroutine(LevelTimeToWinOrLose(levelTime));
                Time.timeScale = 0f;
                break;
            }
            if (superEnemyToDestroy == 0)
            {
                LevelComplete(); 
                victoryPanel.SetActive(true);
                Time.timeScale = 0f;
                break;
            }
            yield return new WaitForSeconds(1f);
        }
    }

    public virtual void UpdateScore()
    {
        scoreText.text = "SCORE: " + score;
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    public void PlayerDead()
    {
        defeatPanel.SetActive(true);
        StopCoroutine(LevelTimeToWinOrLose(levelTime));
        Time.timeScale = 0f;
        isPlayerDead = true;
    }

    public  void DeadSuperEnemyCount()
    {
        superEnemyToDestroy--;
    }

    public int Score
    {
        get { return score; }
    }

    public int TotalScore
    {
        get { return totalScore; }
    }

    private void LevelComplete()
    {
        player.Load();
        player.level++;
        player.score += score;
        player.Save();
        totalScore = player.score;
    }
}
