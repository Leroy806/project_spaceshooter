﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipBase : MonoBehaviour, IHitBox, IDamager 
{
    [SerializeField] private float health = 1f;
    [SerializeField] private float damage = 1f;
    [SerializeField] private float speed = -5f;
    [SerializeField] private GameObject explosion;
    [SerializeField] private float dodge;
    [SerializeField] private float tilt = 3f;
    [SerializeField] private float smothing;
    [SerializeField] private Vector2 startWait;
    [SerializeField] private Vector2 maneuverTime;
    [SerializeField] private Vector2 maneuverWait;
    [SerializeField] private Boundary boundary;
    [SerializeField] private bool isSuperEnemy;


    private float targetManeuver;
    private float currentSpeed;
    private Rigidbody rb;
    private GameController gameController;

    void Start()
    {
        var gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }

        rb = GetComponent<Rigidbody>();
        rb.velocity = Mover.MoveObj(this.gameObject, rb, speed);
        currentSpeed = rb.velocity.z;
        StartCoroutine(Evade());
    }

    public bool IsSuperEnemy
    {
        get => isSuperEnemy;
    }
    public float Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }


    public float Damage
    {
        get => damage;
    }

    public void Hit(float damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        if (isSuperEnemy)
        {
            gameController.DeadSuperEnemyCount();
        }
        var exp = Instantiate(explosion, this.transform.position, this.transform.rotation);
        Destroy(exp, 1f);
        Destroy(this.gameObject);
    }

    private IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

        while (true)
        {
            targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }

    private void FixedUpdate()
    {
        var newManeuver = Mathf.MoveTowards(rb.velocity.x, targetManeuver, Time.deltaTime * smothing);
        rb.velocity = new Vector3(newManeuver, 0f, currentSpeed);
        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
            );
        rb.rotation = Quaternion.Euler(0f, 0f, rb.velocity.x * -tilt);
    }

    public void BossEntersTheScene()
    {
        speed = -1f;
    }
}
