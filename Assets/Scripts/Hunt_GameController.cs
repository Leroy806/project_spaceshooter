﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Hunt_GameController : GameController
{
    [SerializeField] private Text enemyHealthText;
    [SerializeField] private GameObject superEnemyPrefab;
    
   
    private GameObject superEnemy;
    private IHitBox superEnemyHealth;

    void Start()
    {
        superEnemy = Instantiate(superEnemyPrefab, new Vector3(0f, 0f, 13f), Quaternion.identity);
        superEnemyHealth = superEnemy.GetComponent<IHitBox>();
        base.Start();
        StartCoroutine(SpawnWaves());
    }

    


    private IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);

        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                var spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                var spawnRotation = Quaternion.identity;

                var spawnNumber = Random.Range(0, hazards.Length);
                               var hazardSpawn = hazards[spawnNumber];
                ObjectsPool.Instance.GetObject(hazardSpawn, spawnPosition, spawnRotation);
                //Instantiate(hazardSpawn, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);


            if (isPlayerDead)
            {
                break;
            }
        }
    }
    public override void UpdateScore()
    {
        base.UpdateScore();
        enemyHealthText.text = "ENEMY HEALTH: " + superEnemyHealth.Health;
    }

}
